#include <avr/io.h>
#include "avr.h"
#include "lcd.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>


#define LEFT    5
#define RIGHT   7
#define UP      2
#define DOWN    8
#define ENTRY   4
#define COUNTER 16

//char test_var[8];

//Initialize global time units


typedef struct Date {
    unsigned char   month;
    unsigned char   day;  
    unsigned short  year; 
    unsigned char   hour;  
    unsigned char   minute; 
    unsigned char   second; 
    unsigned char   decisecond; 
} Date;

typedef struct Coord {
    unsigned char x;
    unsigned char y;
} Coord;

typedef unsigned char uc;

unsigned char pressed(unsigned char r,unsigned char c){
     
    DDRC=0x00;          //Set DDRC to all input
    PORTC=0xF0;         //Set PORTC3 to 'N/C'
    
	//Set that row to HIGH/'1'
    SET_BIT(DDRC,r);    //Set rth DDR to output
    CLR_BIT(PORTC,r);   //Set rth PORT to '0'
    
	//Set column C to 'Z'
    SET_BIT(PORTC,c+4);
	
    //Return 1 if pressed, 0 if not
    return (GET_BIT(PINC,c+4)) ? 0 : 1;  
}

//Gets the position on the keyboard (r*4 + c + 1)
//by iterating through the buttons and calling
//the *pressed* function
unsigned char get_key(){
    unsigned char r,c;
    for(r=0;r<4;++r){
        for(c=0;c<4;++c){
            if(pressed(r,c)){
                return(r*4 + c + 1);
            }
        }
    }
    return 0;
}

char calculate_maximum_month_day(Date *date){
    //Checks if 28th day of the month 
    if (date->day > 27){
        //Checks if month is Feb. or not
        if (date->month == 2){
            // Conditions for Leap Year:
            // 1) Divisible by 4 and NOT divisible by 100
            //    OR
            // 2) Divisible by 400
            unsigned char is_leap_year = ( ((date->year%4 == 0) && (date->year%100 != 0) ) || (date->year%400 == 0) );
                
            //True if day is >= 29 and is  a leap year (reset the day)
            //True if day is >= 28 and not a leap year (reset the day)
            return ( ((date->day >= 29) && (is_leap_year)) || ((date->day >= 28) && !(is_leap_year)) );
        }
        //Checks if month is Jan. to July (excluding Feb)
        else if (date->month < 8){
            //Checks if month is even
            if (date->month%2 == 0) {return (date->day == 30);}
            //Else month is odd
            else {return (date->day >= 31);}
        }
        else {
            //Checks if month is even
            if (date->month%2 == 0) {return (date->day >= 31);}
            //Else month is odd
            else {return (date->day >= 30);}
        }
    }

    return 0;
}

void map_array_to_exisiting_date(uc *digits, Date *date) {

    //Month
    date->month  = digits[0]*10   + digits[1];
    //Day
    date->day    = digits[2]*10   + digits[3];
    //Year
    date->year   = digits[4]*1000 + digits[5]*100 + digits[6]*10 + digits[7];
    //Hour
    date->hour   = digits[8]*10   + digits[9];
    ////Minute
    date->minute = digits[10]*10  + digits[11];
    //Second
    date->second = digits[12]*10  + digits[13];
    //Decisecond 
    date->decisecond = digits[14];
}

void map_date_to_existing_array(uc *digits, Date *date) {

    //Month
    digits[0]  = date->month/10;
    digits[1]  = date->month%10;
    //Day
    digits[2]  = date->day/10;
    digits[3]  = date->day%10;
    //Year
    digits[4]  = date->year/1000;
    digits[5]  = date->year/100%10;
    digits[6]  = date->year/10%10;
    digits[7]  = date->year%10;
    //Hour
    digits[8]  = date->hour/10;
    digits[9]  = date->hour%10;
    //Minute
    digits[10] = date->minute/10;
    digits[11] = date->minute%10;
    //Second
    digits[12] = date->second/10;
    digits[13] = date->second%10;
    //Decisecond
    digits[14] = date->decisecond;
}





//Used for choosing which digit to modify in 'digits'
void calculate_new_digit_location(uc keyPressed, uc *digitLocation) {
    if (keyPressed == LEFT) {
        switch (*digitLocation) {
            case 0:
                break;
            default:
                --(*digitLocation); }}

    else if (keyPressed == RIGHT) {

        switch (*digitLocation) {
            case 14:
                break;
            default:
                ++(*digitLocation); }}
}

//Used for modifying the value of a given index in 'digits'
void calculate_new_digit_value(uc keyPressed, uc *digitLocation, uc *digits) {

    void print_something(){
        char printBuffer[16];
        sprintf(printBuffer,"Ds: %i%i", digits[14],digits[15]);
        clr_lcd();
        pos_lcd(0,0);
        puts_lcd2(printBuffer);
        wait_avr(500);
    }

    if (keyPressed == UP) {
        switch (*digitLocation) {
        //MONTH.............................
            case 0: //index (position)
                switch (digits[0]) { //value of index
                    case 0:
                        digits[0] = 1;
                        digits[1] = 0;
                        break;
                    case 1:
                        digits[0] = 0;
                        digits[1] = 1;
                        break; }
                break;

            case 1:
                if (digits[0] == 1){
                    switch (digits[1]) {
                        case 2:
                            digits[0] = 0;
                            digits[1] = 1;
                            break;
                        default:
                            ++digits[1]; }}
                else {
                    switch (digits[1]) {
                        case 9:
                            digits[0] = 1;
                            digits[1] = 0;
                            break;
                        default:
                            ++digits[1]; }}
                break;
        //DAY...............................
            case 2:
                switch (digits[2]) {
                    case 3:
                        digits[2] = 0;
                        digits[3] = 1;
                        break;
                    case 2:
                        if (digits[3] > 1) digits[3] = 0;
                        digits[2] = 3;
                        break;
                    default:
                        ++digits[2]; }
                break;

            case 3:
                if (digits[2] != 3) {
                    switch (digits[3]) {
                        case 9:
                            digits[3] = 0;
                            ++digits[2];
                            break;
                        default:
                            ++digits[3]; }}
                else {
                    switch (digits[3]) {
                        case 0:
                            digits[3] = 1;
                            break;
                        case 1:
                            digits[2] = 0;
                            digits[3] = 1; }}
                break;
        //YEAR..............................
            case 4:
                switch (digits[4]) {
                    case 9:
                        digits[4] = 0;
                        digits[5] = 0;
                        digits[6] = 0;
                        digits[7] = 1;
                        break;
                    default:
                        ++digits[4]; }
                break;

            case 5:
                switch (digits[5]) {
                    case 9:
                        if (digits[4] + digits[6] + digits[7]) {
                            digits[5] = 0; }
                        break;
                    default:
                      ++digits[5]; }
                break;

            case 6:
                switch (digits[6]) {
                    case 9:
                        if (digits[4] + digits[5] + digits[7]) {
                            digits[6] = 0; }
                       break;
                   default:
                       ++digits[6]; }
                break;
            case 7:
                switch (digits[7]) {
                    case 9:
                         if (digits[4] + digits[5] + digits[6]) {
                            digits[7] = 0; }
                        break;
                    default:
                        ++digits[7]; }
                break;
        //HOUR..............................
            case 8:
                switch (digits[8]) {
                    case 2:
                        digits[8] = 0;
                        digits[9] = 0;
                        break;
                    default:
                        ++digits[8]; }
                break;

            case 9:
                if (digits[8] != 2) {
                    switch (digits[9]) {
                        case 9:
                            digits[9] = 0;
                            ++digits[8];
                            break;
                        default:
                            ++digits[9]; }}
                else {
                    switch (digits[9]) {
                        case 3:
                            digits[8] = 0;
                            digits[9] = 0;
                            break;
                        default:
                            ++digits[9]; }}
                            
                break;
        //MINUTES...........................
            case 10:
                switch (digits[10]) {
                    case 5:
                        digits[10] = 0;
                        break;
                    default:
                        ++digits[10]; }
                break;
            case 11:
                switch (digits[11]) {
                    case 9:
                        digits[11] = 0;
                        break;
                    default:
                        ++digits[11]; }
                break;
        //SECONDS...........................
          case 12:
                switch (digits[12]) {
                    case 5:
                        digits[12] = 0;
                        break;
                    default:
                        ++digits[12]; }
                break;
            case 13:
                switch (digits[13]) {
                    case 9:
                        digits[13] = 0;
                        break;
                    default:
                        ++digits[13]; }
                break;
        //DECISECONDS.......................
            case 14:
                switch (digits[14]) {
                    case 9:
                        digits[14] = 0;
                        break; 
                    default:
                        ++digits[14]; }
                break;

        } //outmost switch
    } //outmost if
}

//Used to calculate where to move on the LCD screen
void calculate_new_coord(uc keyPressed, Coord* coord) {
    if (keyPressed == LEFT) {
        if (coord->y == 0) {
            switch (coord->x) {
                case 0  :
                    break;
                case 3  :
                    coord->x -= 2;
                    break;
                case 6  :
                    coord->x -= 2;
                    break;
                default :
                    --coord->x; }}
        else {
            switch (coord->x) {
                case 0 :
                    coord->x = 9;
                    coord->y = 0;
                    break;
                case 3 :
                    coord->x -= 2;
                    break;
                case 6 :
                    coord->x -= 2;
                    break;
                case 9:
                    coord->x -= 2;
                    break;
                default:
                    --coord->x; }}}

    else if (keyPressed == RIGHT) {
        if (coord->y == 0) {
            switch (coord->x) {
                case 1  :
                    coord->x += 2;
                    break;
                case 4  :
                    coord->x += 2;
                    break;
                case 9  :
                    coord->x = 0;
                    coord->y = 1;
                    break;
                default :
                    ++coord->x; }}
        else {
            switch (coord->x) {
                case 1 :
                    coord->x += 2;
                    break;
                case 4 :
                    coord->x += 2;
                    break;
                case 7 :
                    coord->x += 2;
                    break;
                case 9:
                    break;
                default:
                    ++coord->x; }}}
}



//Increase the smallest time unit
void increment_date(Date *date){
        if((*date).decisecond == 9){
            (*date).decisecond = 0;

            if((*date).second == 59){
                (*date).second = 0;
                                                                          
                if((*date).minute == 59){                                        
                    (*date).minute = 0;

                    if((*date).hour == 23){                                     
                        (*date).hour = 0;
                        
                        if(calculate_maximum_month_day(date)){  //TODO: implement logic depending
                            (*date).day = 1;   

                            if((*date).month == 12){
                                ++(*date).year;
                                (*date).month = 1;
                            } else {++(*date).month;}       


                        } else {++(*date).day;}              
                    } else {++(*date).hour;}          
                } else {++(*date).minute;}                       
            } else {++(*date).second;}
        } else {++(*date).decisecond;}                           
}

void wait_and_increment_date(Date *date){
    wait_avr(100);
    increment_date(date);
}

void blink_test(Coord *coord, Date* date) {
    //char c[10];
    //sprintf(c,"%i", keyPressed);
    pos_lcd(coord->y,coord->x);
    put_lcd(' ');
    wait_avr(100);
}

int main(void){

//Year rollover test
//unsigned short month=12, day=31, year=2016, hour=23, minute=59, second=58, decisecond=0;

//Month rollover test (31st)
//unsigned short month=11, day=30, year=2016, hour=23, minute=59, second=58, decisecond=0;

//Month rollover test (30th)
//unsigned short month=1, day=30, year=2016, hour=23, minute=59, second=58, decisecond=0;

//Day rollover test
//unsigned short month=11, day=29, year=2016, hour=23, minute=59, second=58, decisecond=0;

//Hour rollover test
//unsigned short month=11, day=30, year=2016, hour=22, minute=59, second=58, decisecond=0;

//Minute rollover test
//unsigned short month=11, day=30, year=2016, hour=23, minute=59, second=58, decisecond=0;

//Minute rollover test
//unsigned short month=11, day=30, year=2016, hour=23, minute=58, second=58, decisecond=0;

//Second rollover test
//unsigned short month=11, day=30, year=2016, hour=23, minute=59, second=50, decisecond=0;

//Leap year test
//unsigned short month=2, day=28, year=2016, hour=23, minute=59, second=58, decisecond=0;

//Not leap year test
unsigned short month=2, day=30, year=2100, hour=23, minute=59, second=58, decisecond=0;

    //Initialize date object
    Date date = {month,day,year,hour,minute,second,decisecond};
	
	
    //Initialize Counter object
    Date counter = {0,0,0,0,0,0,0};
    //Initiate Coord object for entry mode (used for visible "blinker")
    Coord coord = {0,0};
    //Initiate array of uc's for date mapping
    uc digits[15];// = malloc( 18 * sizeof(uc));
    //Initiate value for digit location in digits (used in entry mode)
    uc digitL = 0;
    uc *digitLocation = &digitL;

    //Initialize and clear lcd
    ini_lcd();
    clr_lcd();
    //Initialize lcd print buffer
    char firstRow[16];
    char secondRow[16];

    //Initiaze entry   mode variable
    uc entryMode = 0;
    //Initiaze counter mode variable
    uc counterMode = 0;


    //Start logic for LCD
    while (1){
        //Wait 0.1 seconds
        wait_avr(100);
        //Increase the global time units
        increment_date(&date);

        //Do an initial back and forth mapping
        void date_to_array(){
            map_date_to_existing_array(digits, &date); }
        void array_to_date(){
            map_array_to_exisiting_date(digits, &date); }

        date_to_array();
        array_to_date();



        //Format the first and second year

        //Clear lcd
        clr_lcd();

        //Put the first and second rows onto the lcd
        void print_date_time() {
            sprintf(firstRow,  "%02hu/%02hu/%04hu",         date.month, date.day,     date.year                   );
            sprintf(secondRow, "%02hu:%02hu:%02hu:%hu0",    date.hour,  date.minute,  date.second, date.decisecond);
            pos_lcd(0,0);
            puts_lcd2(firstRow);
            pos_lcd(1,0);
            puts_lcd2(secondRow);
        }
        print_date_time();


        //ENTRY MODE
        uc keyPressed = get_key();
        if (keyPressed == ENTRY) {
            entryMode = 1;
            wait_avr(300); }
        else if (keyPressed == COUNTER) {
            //Initialize counter object
            counterMode = 1;
            wait_avr(100); }
                
        while (entryMode == 1) {   
            keyPressed = get_key();
            if (keyPressed == ENTRY) {
                wait_avr(300);
                entryMode = 0; }
            else {
                if (keyPressed){
                    switch (keyPressed) {
                        case LEFT:
                        case RIGHT:
                            calculate_new_digit_location(keyPressed, digitLocation);
                            calculate_new_coord(keyPressed, &coord);
                            blink_test(&coord, &date);
                           break;
                        case UP:
                            calculate_new_digit_value(keyPressed, digitLocation, digits);
                            array_to_date();
                        case DOWN:
                            break; }
                print_date_time();
                wait_avr(100); }}
        }


        while (counterMode == 1) {
            keyPressed = get_key();
            if(keyPressed == COUNTER) {
                counterMode = 0;
                //Reset counter
                counter.hour = counter.minute = counter.second = counter.decisecond = 0; }

            //Increate both date and counter times
            wait_avr(100);
            increment_date(&date);
            increment_date(&counter);

            //Counter print
            clr_lcd();
            char upper[16];
            char lower[16];
            sprintf(upper,  "HR MIN SEC");
            sprintf(lower,  "%02hu  %02hu %02hu:%hu0", counter.hour, counter.minute, counter.second, counter.decisecond);
            pos_lcd(0,0);
            puts_lcd2(upper);
            pos_lcd(1,0);
            puts_lcd2(lower);

        }

        
    }
}

