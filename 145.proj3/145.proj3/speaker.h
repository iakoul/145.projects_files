/*
 * speaker.h
 *
 * Created: 5/8/2016 2:29:11 PM
 *  Author: zhong
 */ 


#ifndef SPEAKER_H_
#define SPEAKER_H_

typedef struct Freq {
	unsigned int frequency_100;
	unsigned char  rounded_duration;
	
} Freq;

typedef struct Note {
	Freq* freq;
	unsigned short duration;
} Note;


typedef struct Song {
	unsigned char name[32];
	Note** notes;
	unsigned char length;
} Song;

void play_note(Note* note);
void play_song(Song* song);

#endif /* SPEAKER_H_ */