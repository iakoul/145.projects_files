/*
 * speaker.c
 *
 * Created: 5/8/2016 2:17:45 PM
 *  Author: zhong
 */ 

#include <avr/io.h>
#include "avr.h"
#include "speaker.h"



void play_note(Note* note){
	for (int i = 0; i < note->duration; ++i){
		SET_BIT(PORTA,0);
		wait_avr(note->freq->rounded_duration);
		CLR_BIT(PORTA,0);
		wait_avr(note->freq->rounded_duration);
	}
}

void play_freq(Freq* freq, unsigned short duration){
	for (unsigned short i = 0; i < duration; ++i){
		SET_BIT(PORTA,0);
		wait_avr(freq->rounded_duration);
		CLR_BIT(PORTA,0);
		wait_avr(freq->rounded_duration);
	}
}

void play_song(Song* song){
	for (unsigned short i = 0; i < song->length; ++i) {
		play_note((song->notes)[i]);
	}
}
