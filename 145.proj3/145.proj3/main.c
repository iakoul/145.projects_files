/*
 * 145.proj3.c
 *
 * Created: 5/8/2016 1:48:26 PM
 * Author : Zhong
 */ 

#include <avr/io.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "avr.h"
#include "lcd.h"

#include "keypad.h"
#include "speaker.h"

#define eighth  50
#define quarter 100
#define half    200

int main(void) {
        //Initialize and clear lcd
        ini_lcd();
        clr_lcd();
        
        DDRA = 0x01;
        //puts_lcd2("Sounds");



//Set frequencies and values
//---------------------------------------------------------------------
    Freq fs;
    
    Freq A7,A7s,B7,C8,C8s,D8,D8s,E8,F8,F8s,G8,G8s;
    
    Freq C0,Cs0,D0,Ds0,E0,F0,Fs0,G0,Gs0,A0,As0,B0;
    Freq C,Cs,D,Ds,E,F,Fs,G,Gs,A,As,B;
    Freq C1,Cs1,D1,Ds1,E1,F1,Fs1,G1,Gs1,A1,As1,B1;
    
//---------------------------------------------------------------------

   fs.rounded_duration = 0;

   //Octave -1
      A7.rounded_duration = 90;
     A7s.rounded_duration = 86;
      B7.rounded_duration = 80;
      C8.rounded_duration = 76;
     C8s.rounded_duration = 72;
      D8.rounded_duration = 68;
     D8s.rounded_duration = 64;
      E8.rounded_duration = 61;
      F8.rounded_duration = 57;
     F8s.rounded_duration = 54;
      Gs.rounded_duration = 51;
     G8s.rounded_duration = 48;


    //Octave 0
     C0.frequency_100 = 13081;   C0.rounded_duration = 38;
    Cs0.frequency_100 = 13859;  Cs0.rounded_duration = 36;
     D0.frequency_100 = 14683;   D0.rounded_duration = 34;
    Ds0.frequency_100 = 15556;  Ds0.rounded_duration = 32;
     E0.frequency_100 = 16481;   E0.rounded_duration = 30;
     F0.frequency_100 = 17461;   F0.rounded_duration = 29;
    Fs0.frequency_100 = 18500;  Fs0.rounded_duration = 27;
     G0.frequency_100 = 19600;   G0.rounded_duration = 26;
    Gs0.frequency_100 = 20765;  Gs0.rounded_duration = 24;
     A0.frequency_100 = 22000;   A0.rounded_duration = 23;
    As0.frequency_100 = 23308;  As0.rounded_duration = 21;
     B0.frequency_100 = 24694;   B0.rounded_duration = 20;
    //Octave
     C.frequency_100 = 26163;   C.rounded_duration = 19;
    Cs.frequency_100 = 27718;  Cs.rounded_duration = 18;
     D.frequency_100 = 29366;   D.rounded_duration = 17;
    Ds.frequency_100 = 31113;  Ds.rounded_duration = 16;
     E.frequency_100 = 32963;   E.rounded_duration = 15;
     F.frequency_100 = 34923;   F.rounded_duration = 14;
    Fs.frequency_100 = 36999;  Fs.rounded_duration = 14;
     G.frequency_100 = 39200;   G.rounded_duration = 13;
    Gs.frequency_100 = 41530;  Gs.rounded_duration = 12;
     A.frequency_100 = 44000;   A.rounded_duration = 11;
    As.frequency_100 = 46616;  As.rounded_duration = 11;
     B.frequency_100 = 49388;   B.rounded_duration = 10;
     
    //Octave 1
     C1.frequency_100 = 52325;   C1.rounded_duration = 9;
    Cs1.frequency_100 = 55437;  Cs1.rounded_duration = 9;
     D1.frequency_100 = 58733;   D1.rounded_duration = 8;
    Ds1.frequency_100 = 62225;  Ds1.rounded_duration = 8;
     E1.frequency_100 = 65925;   E1.rounded_duration = 7;
     F1.frequency_100 = 69846;   F1.rounded_duration = 7;
    Fs1.frequency_100 = 73999;  Fs1.rounded_duration = 7;
     G1.frequency_100 = 78399;   G1.rounded_duration = 6;
    Gs1.frequency_100 = 83061;  Gs1.rounded_duration = 6;
     A1.frequency_100 = 88000;   A1.rounded_duration = 6;
    As1.frequency_100 = 93233;  As1.rounded_duration = 5;
     B1.frequency_100 = 98777;   B1.rounded_duration = 5;
//---------------------------------------------------------------------
    Note ns;
    
    Note C8_e,C8_q,C8_h,
         D8_e,D8_q,D8_h,
         E8_e,E8_q,E8_h,
         F8_e,F8_q,F8_h,
         F8s_e,F8s_q,F8s_h,
         G8_e,G8_q,G8_h,
         A7_e,A7_q,A7_h,
         B7_e,B7_q,B7_h;    

    Note C0_e,C0_q,C0_h,
         D0_e,D0_q,D0_h,
         E0_e,E0_q,E0_h,
         F0_e,F0_q,F0_h,
         G0_e,G0_q,G0_h,
         A0_e,A0_q,A0_h,
         B0_e,B0_q,B0_h;

    Note C_e,C_q,C_h,
         D_e,D_q,D_h,
         E_e,E_q,E_h,
         F_e,F_q,F_h,
         Fs_e,Fs_q,Fs_h,
         G_e,G_q,G_h,
         A_e,A_q,A_h,
         B_e,B_q,B_h;

    Note C1_e,C1_q,C1_h,
         D1_e,D1_q,D1_h,
         E1_e,E1_q,E1_h,
         F1_e,F1_q,F1_h,
         G1_e,G1_q,G1_h,
         A1_e,A1_q,A1_h,
         B1_e,B1_q,B1_h;
//---------------------------------------------------------------------
    ns.freq = &fs;
    ns.duration = 1000;

    //A7------------------------------------------
    C8_e.freq = &C8; C8_e.duration = eighth;
    C8_q.freq = &C8; C8_q.duration = quarter;
    C8_h.freq = &C8; C8_h.duration = half;

    //D8
    D8_e.freq = &D8; D8_e.duration = eighth;
    D8_q.freq = &D8; D8_q.duration = quarter;
    D8_h.freq = &D8; D8_h.duration = half;

    //E8
    E8_e.freq = &E8; E8_e.duration = eighth;
    E8_q.freq = &E8; E8_q.duration = quarter;
    E8_h.freq = &E8; E8_h.duration = half;

    //F8   
    F8_e.freq = &F8; F8_e.duration = eighth;
    F8_q.freq = &F8; F8_q.duration = quarter;
    F8_h.freq = &F8; F8_h.duration = half;

    //F8s
    F8s_e.freq = &F8; F8s_e.duration = eighth;
    F8s_q.freq = &F8; F8s_q.duration = quarter;
    F8s_h.freq = &F8; F8s_h.duration = half;

    //G8   
    G8_e.freq = &G8; G8_e.duration = eighth;
    G8_q.freq = &G8; G8_q.duration = quarter;
    G8_h.freq = &G8; G8_h.duration = half;

    //A7   
    A7_e.freq = &A7; A7_e.duration = eighth;
    A7_q.freq = &A7; A7_q.duration = quarter;
    A7_h.freq = &A7; A7_h.duration = half;

    //B7  
    B7_e.freq = &B7; B7_e.duration = eighth;
    B7_q.freq = &B7; B7_q.duration = quarter;
    B7_h.freq = &B7; B7_h.duration = half;
  

    //C0------------------------------------------
    C0_e.freq = &C0; C0_e.duration = eighth;
    C0_q.freq = &C0; C0_q.duration = quarter;
    C0_h.freq = &C0; C0_h.duration = half;

    //D0   
    D0_e.freq = &D0; D0_e.duration = eighth;
    D0_q.freq = &D0; D0_q.duration = quarter;
    D0_h.freq = &D0; D0_h.duration = half;

    //E0
    E0_e.freq = &E0; E0_e.duration = eighth;
    E0_q.freq = &E0; E0_q.duration = quarter;
    E0_h.freq = &E0; E0_h.duration = half;

    //F0   
    F0_e.freq = &F0; F0_e.duration = eighth;
    F0_q.freq = &F0; F0_q.duration = quarter;
    F0_h.freq = &F0; F0_h.duration = half;

    //G0   
    G0_e.freq = &G0; G0_e.duration = eighth;
    G0_q.freq = &G0; G0_q.duration = quarter;
    G0_h.freq = &G0; G0_h.duration = half;

    //A0   
    A0_e.freq = &A0; A0_e.duration = eighth;
    A0_q.freq = &A0; A0_q.duration = quarter;
    A0_h.freq = &A0; A0_h.duration = half;

    //B0   
    B0_e.freq = &B0; B0_e.duration = eighth;
    B0_q.freq = &B0; B0_q.duration = quarter;
    B0_h.freq = &B0; B0_h.duration = half;

    //C-------------------------------------------

    C_e.freq = &C; C_e.duration = eighth;
    C_q.freq = &C; C_q.duration = quarter;
    C_h.freq = &C; C_h.duration = half;

    //D    
    D_e.freq = &D; D_e.duration = eighth;
    D_q.freq = &D; D_q.duration = quarter;
    D_h.freq = &D; D_h.duration = half;

    //E    
    E_e.freq = &E; E_e.duration = eighth;
    E_q.freq = &E; E_q.duration = quarter;
    E_h.freq = &E; E_h.duration = half;

    //F    
    F_e.freq = &F; F_e.duration = eighth;
    F_q.freq = &F; F_q.duration = quarter;
    F_h.freq = &F; F_h.duration = half;
    
    //Fs
    Fs_e.freq = &F; Fs_e.duration = eighth;
    Fs_q.freq = &F; Fs_q.duration = quarter;
    Fs_h.freq = &F; Fs_h.duration = half;
    
    //G    
    G_e.freq = &G; G_e.duration = eighth;
    G_q.freq = &G; G_q.duration = quarter;
    G_h.freq = &G; G_h.duration = half;

    //A    
    A_e.freq = &A; A_e.duration = eighth;
    A_q.freq = &A; A_q.duration = quarter;
    A_h.freq = &A; A_h.duration = half;

    //B    
    B_e.freq = &B; B_e.duration = eighth;
    B_q.freq = &B; B_q.duration = quarter;
    B_h.freq = &B; B_h.duration = half;

    //C1------------------------------------------

    C1_e.freq = &C1; C1_e.duration = eighth;
    C1_q.freq = &C1; C1_q.duration = quarter;
    C1_h.freq = &C1; C1_h.duration = half;

    //D1   
    D1_e.freq = &D1; D1_e.duration = eighth;
    D1_q.freq = &D1; D1_q.duration = quarter;
    D1_h.freq = &D1; D1_h.duration = half;

    //E1   
    E1_e.freq = &E1; E1_e.duration = eighth;
    E1_q.freq = &E1; E1_q.duration = quarter;
    E1_h.freq = &E1; E1_h.duration = half;

    //F1   
    F1_e.freq = &F1; F1_e.duration = eighth;
    F1_q.freq = &F1; F1_q.duration = quarter;
    F1_h.freq = &F1; F1_h.duration = half;

    //G1   
    G1_e.freq = &G1; G1_e.duration = eighth;
    G1_q.freq = &G1; G1_q.duration = quarter;
    G1_h.freq = &G1; G1_h.duration = half;

    //A1   
    A1_e.freq = &A1; A1_e.duration = eighth;
    A1_q.freq = &A1; A1_q.duration = quarter;
    A1_h.freq = &A1; A1_h.duration = half;

    //B1   
    B1_e.freq = &B1; B1_e.duration = eighth;
    B1_q.freq = &B1; B1_q.duration = quarter;
    B1_h.freq = &B1; B1_h.duration = half;

//---------------------------------------------------------------------

    //Song
    Song HB2U;
    Note* HB2U_notes[25] = {&C0_e,&C0_e,&D0_q,&C0_q,&F0_q,&E0_h,
                            &C0_e,&C0_e,&D0_q,&C0_q,&F0_q,&E0_h,
                            &C0_e,&C0_e,&C_q,&A0_q,&F0_q,&E0_q,&D0_q,
                            &B0_e,&B0_e,&A0_q,&F0_q,&G0_q,&F0_h};
    HB2U.length = 25;
    HB2U.notes = &HB2U_notes;
    strcpy(HB2U.name, "Happy Birthday to You");

    Song IM;
    Note* IM_notes[19] = {&B0_h,&D_h,&ns,&D_q,&E_q,&ns,&E_h,
                          &G_e,&F_e,&G_e,&F_e,&G_e,&F_e,
                          &D_q,&ns,&D_q,&E_q,&ns,&E_h};
    IM.notes = &IM_notes;
    IM.length = 19;
    strcpy(IM.name, "Iron Man Intro");

    Song FF;
    Note* FF_notes[14] = {&D1_e,&ns,&D1_e,&ns,&D1_e,&ns,&D1_q,
                          &B_h,&C1_q,&D1_e,&C1_e,&D1_h,&D1_h,&D1_h};

    FF.notes = &FF_notes;
    FF.length = 14;
    strcpy(FF.name, "Final Fantasy Fanfare Intro");
    
    Song J2TW;
    Note* J2TW_notes[22] = {&C1_h,&C1_h,&B_q,&A_q,&G_h,&G_q,
                            &F_q,&E_h,&D_h,&C_h,&C_q,
                            &G_q,&A_h,&A_h,&ns,&A_q,&B_h,&B_h,
                            &ns,&B_q,&C1_h,&C1_h};

    J2TW.notes = &J2TW_notes;
    J2TW.length = 22;
    strcpy(J2TW.name, "Joy to the World");
    
   print_menu();
    while(1) {
        unsigned char symbolPressed = keypad_symbol_pressed();
		wait_avr(1000);

        switch (symbolPressed) {
            case 'A':
                pos_lcd(0,0);
                clr_lcd();

                puts_lcd2("Happy Birthday");
                pos_lcd(1,0);
                puts_lcd2("to You");

                play_song(&HB2U);
                clr_lcd();
				print_menu();
                break;

            case 'B':
                pos_lcd(0,0);
                clr_lcd();

                puts_lcd2("Iron Man");
                pos_lcd(1,0);
                puts_lcd2("Intro");


                play_song(&IM);
                clr_lcd();
				print_menu();
                break;
                
            case 'C':
                pos_lcd(0,0);
                clr_lcd();
                
                puts_lcd2("Final Fantasy");
                pos_lcd(1,0);
                puts_lcd2("Intro");

                play_song(&FF);

                clr_lcd();
				print_menu();
                break;
				
            case 'D':
                pos_lcd(0,0);
                clr_lcd();

                puts_lcd2("Joy to the");
                pos_lcd(1,0);
                puts_lcd2("World");

                play_song(&J2TW);
                clr_lcd();
				print_menu();
				
                break;
            case 'Z':
                break;        
       }
 
   }
}

void print_menu() {
			pos_lcd(0,0);
			clr_lcd();
			puts_lcd2("1: HB2U  2: Iron Man");
			pos_lcd(1,0);
			puts_lcd2("3: FF    4: J2TW");
}
