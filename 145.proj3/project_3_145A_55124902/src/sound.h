/*
 * sound.h
 *
 * Created: 4/29/2015 3:09:46 PM
 *  Author: Danny
 */ 
#ifndef SOUND_H
#define SOUND_H

struct note
{
	unsigned char freq;
	unsigned char dur;
};

void play_note(unsigned char freq, unsigned char dur);
void play_song(struct note song[], int N);

#endif