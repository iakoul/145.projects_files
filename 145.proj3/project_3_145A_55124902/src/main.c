/**
 * \file
 *
 * \brief Empty user application template
 *
 */

/**
 * \mainpage User Application template doxygen documentation
 *
 * \par Empty user application template
 *
 * Bare minimum empty user application template
 *
 * \par Content
 *
 * -# Include the ASF header files (through asf.h)
 * -# "Insert system clock initialization code here" comment
 * -# Minimal main function that starts with a call to board_init()
 * -# "Insert application code here" comment
 *
 */

/*
 * Include header files for all drivers that have been imported from
 * Atmel Software Framework (ASF).
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */
#include <asf.h>
#include "avr.h"
#include "sound.h"
#include <util/delay.h>
#include "lcd.h"
#include <stdio.h>

#define SI 0
#define A 1
#define As 2
#define B 3
#define C 4
#define Cs 5
#define D 6
#define Ds 7
#define E 8
#define F 9
#define Fs 10
#define G 11
#define Gs 12

#define W 0
#define H 1
#define Q 2
#define E 3

//note_t *song_array;
//song_array = malloc(5*sizeof(*song_array));

//
//
struct note song_array4[12] = {
	{A,W}, {As,W}, {B,W}, {C,W}, {Cs,W}, {D,W}, {Ds,W}, {E,W}, {F,W}, {Fs,W}, {G,W}, {Gs,W}
};
//struct note song_array[3] = {
	////{C,Q},
	////{G,W},
	////{B,Q},
	////{SI,W}
	 //{C,H},
     //{Cs,H},
     //{D,H}
//};
//
struct note song_array2[26] = {
	{B,H},{A,H},{G,H},{A,H},{B,H},{B,H},
	{B,H},{A,H},{A,H},{A,H},{B,H},{D,H},{D,H},
	{B,H},{A,H},{G,H},{A,H},{B,H},{B,H},{B,H},
	{B,H},{A,H},{A,H},{B,H},{A,H},{G,H},
};
//
////struct note song_array2[26] = {
	////{C,H}, {As,H},{Gs,H},{As,H},{Cs,H},{Cs,H},
	////{C,H}, {As,H},{As,H},{As,H},{Cs,H},{D,H},{D,H},
	////{C,H},{As,H},{Gs,H},{As,H},{Cs,H},{Cs,H},{Cs,H},
	////{C,H},{As,H},{As,H},{Cs,H},{As,H},{Gs,H},
////};
//
//
//struct note song_array3[42] = {
	//{C,H}, {C,H},{G,H},{G,H},{A,H},{A,H},
	//{G,H}, {F,H},{F,H},{E,H},{E,H},{D,H},{D,H},
	//{C,H},{G,H},{G,H},{F,H},{F,H},{E,H},{E,H},
	//{D,H},{G,H},{G,H},{F,H},{F,H},{E,H}, {E,H},
	//{D,H}, {C,H}, {C,H}, {G,H}, {G,H}, {A,H},
	//{A,H}, {G,H}, {F,H}, {F,H}, {E,H}, {E,H},
	//{D,H}, {D,H}, {C,H}
//};
//
//struct note song_array4[12] = {
	//{A,W}, {As,W}, {B,W}, {C,W}, {Cs,W}, {D,W}, {Ds,W}, {E,W}, {F,W}, {Fs,W}, {G,W}, {Gs,W}
	//};
	//
//struct note song_array5[12] = {
	//{C,H}, {Cs,H}, {D,H}, {Ds,H}, {E,H}, {F,H}, {Fs,H}, {G,H}, {Gs,H}, {A,H}, {As,H}, {B,H}
	//};
	//
//struct note song_array6[11] = {
	//{E,H}, {E,H},{E,H},{C,Q},{E,Q},{G,Q},
	//{C,Q}, {Gs,Q}, {C,Q}, {G,H}, {E,H},
//};

int main (void)
{
	/* Insert system clock initialization code here (sysclk_init()). */

	board_init();
	ini_lcd();
	clr_lcd();
	SET_BIT(DDRB,3);
	CLR_BIT(PORTB,3);
	
	//DDRC = 0x00;
	//PORTC = 0xFF;
	
	SET_BIT(PORTC, 1);
	CLR_BIT(DDRC, 1);
	
	CLR_BIT(PORTC, 4);
	SET_BIT(DDRC, 4);
	// SET_BIT(DDRB,3);
	// CLR_BIT(PORTB,3);
	
	char *name = 'A';
	for(;;)
	{
		//play_song(song_array2, 26);
		for(int c = 4; c < 8; c++)
		{
			SET_BIT(DDRC, c);
			CLR_BIT(PORTC, c);
			
			for(int r = 0; r < 4; r++)
			{
				SET_BIT(PORTC, r);
				CLR_BIT(DDRC, r);
				if(!GET_BIT(PINC, r))
				{
					switch(r)
					{
						case 0:
							switch(c)
							{
								case 4:
									clr_lcd();
									pos_lcd(0,0);
							
									//sprintf(name, "%s", 'Mary');
									put_lcd('1');
									play_song(song_array2, 26);

									break;
							}
							break;

						case 1:
							switch(c)
							{
								case 4:
									clr_lcd();
									pos_lcd(0,0);

									//sprintf(name, "%s", 'Twinkle');
									//put_lcd('2');
									play_song(song_array4, 12);
									break;
							}
							break;
					}
				}
			}
		}
	}
	/* Insert application code here, after the board has been initialized. */
}
