/*
 * sound.c
 *
 * Created: 4/29/2015 3:09:19 PM
 *  Author: Danny
 */ 
#include "sound.h"
#include "avr.h"
#include <util/delay.h>

void play_note(unsigned char freq, unsigned char dur)
{
	int i, n;
	double f, d;
	
	switch(freq)
	{
		case 0:
			d = 1;
			break;
		case 1:
			d = 440.0;
			break;
		case 2:
			d = 466.16;
			break;
		case 3:
			d = 493.88;
			break;
		case 4:
			d = 261.63;
			break;
		case 5:
			d = 277.18;
			//d = 293.66;
			break;
		case 6:
			d = 293.66;
			break;
		case 7:
			d = 311.13;
			break;
		case 8:
			d = 329.63;
			break;
		case 9:
			d = 349.23;
			break;
		case 10:
			d = 369.99;
			break;
		case 11:
			d = 392.00;
			break;
		case 12:
			d = 415.30;
			break;
	}
	
	f = ((1/d)/2)/.0001;
	
	switch(dur)
	{
		case 0:
			n = 1*d;
			break;
		case 1:
			n = 0.5*d;
			break;
		case 2:
			n = 0.25*d;
			break;
		case 3:
			n = 0.125*d;
			break;
	}
	
	if(d == 1)
	{
		switch(dur)
		{
			case 0:
				f = 10;
				break;
			case 1:
				f = 5;
				break;
			case 2:
				f = 3;
				break;
			case 3:
				f = 1;
				break;
		}
		
		
		for(i = 0; i < f; ++i)
		{
			//SET_BIT(PORTB,3);
			//wait_avr(100);
			CLR_BIT(PORTB,3);
			wait_avr(100);
		}
	}
	
	else
	{
		for(i = 0; i < n; ++i)
		{
			SET_BIT(PORTB,3);
			wait_avr(f);
			CLR_BIT(PORTB,3);
			wait_avr(f);
		}
		wait_avr(300);
	}
	
}

void play_song(struct note song[], int N)
{
	int i;
	for(i = 0; i < N; i++)
	{
		play_note(song[i].freq, song[i].dur);
	}
}

