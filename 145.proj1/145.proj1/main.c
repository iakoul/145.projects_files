/*
 * 145.proj1.c
 *
 * Created       : 4/7/2016  10:10:46 AM
 * Last Modified : 4/13/2016 10:06:03 AM
 * Author        : Aaron Zhong
 * Course        : CSE 145A
 * Instructor    : Tony Givargis
 */ 

#include <avr/io.h>
#include "avr.h"

int main(void)
{
    DDRB = 1;                       //Set DDRB0 to be an output pin
	for(;;)
	{
		if (!GET_BIT(PINB, 1))
		{
			SET_BIT(PORTB, 0);
			wait_avr(500);          //Use the built-in wait to synchronize the time with the crystal
			//wait();               
			CLR_BIT(PORTB, 0);	    
			//wait();               //Use the manually created wait function 
			wait_avr(500);
		}
	}

}

void wait()                         //Manually created wait function
{
    volatile int i = 0 , m = 10000;
    for(; i < m ; i++ );            //do "nothing"

}

